<?php
add_action( 'cmb2_admin_init', 'looks_yourprefix_register_demo_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function looks_yourprefix_register_demo_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_look_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_demo = new_cmb2_box( array(
		'id'            => $prefix . 'metabox2',
		'title'         => __( 'Configure your look' ),
		'object_types'  => array( 'look', )
	) );
	
	$cmb_demo->add_field( array(
		'name' => __( 'Main Image', 'cmb2' ),
		'desc' => __( 'Specifies a main image', 'cmb2' ),
		'id'   => $prefix . 'main-image',
		'type' => 'file',
	) );

	$cmb_demo->add_field( array(
		'name' => __( 'Additional Image', 'cmb2' ),
		'desc' => __( 'Specifies an additional image', 'cmb2' ),
		'id'   => $prefix . 'additional-image',
		'type' => 'file',
	) );
}