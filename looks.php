<?php
/*
Plugin Name: Looks
Plugin URI: http://sputnikmobile.com/
Description: Looks plugin
Version: 1.2
Author: the Sputnik Mobile team
Author URI: http://sputnikmobile.com/
Bitbucket Plugin URI: https://bitbucket.org/fishtag/looks
Bitbucket Branch: master
*/

/*Some Set-up*/
define('LOOKS_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );

/*-------------------------------------------------------------------------------*/
/*   Register Custom Post Types
/*-------------------------------------------------------------------------------*/
add_action( 'init', 'looks_create_post_type' );
function looks_create_post_type() {
  register_post_type( 'look',
    array(
      'labels' => array(
        'name' => 'Looks',
        'singular_name' => 'Look',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Look',
        'edit' => 'Edit',
        'edit_item' => 'Edit Look',
        'new_item' => 'New Look',
        'view' => 'View',
        'view_item' => 'View Look',
        'search_items' => 'Search Looks',
        'not_found' => 'No Looks found',
        'not_found_in_trash' => 'No Looks found in Trash',
        'parent' => 'Parent Look'
      ),
      'public' => true,
      'rewrite' => array( 'slug' => 'look' ),
      'menu_icon' => 'dashicons-visibility'
    )
  );
}

/*-------------------------------------------------------------------------------*/
/*   CMB2
/*-------------------------------------------------------------------------------*/
include_once('inc/cmb2/fields.php');

/*-------------------------------------------------------------------------------*/
/* Lets register our shortcode
/*-------------------------------------------------------------------------------*/
function looks_cpt_content_func($atts){
  extract( shortcode_atts( array(

    'id' => null,

  ), $atts ) );

  ?>
  <?php ob_start();?>

  <?php
    if ( $lookImageID = get_post_meta( $id, '_look_additional-image_id', 1 ) ) :
      /* $lookProducts = get_field('look_products', $id); */ ?>
      <aside class="insertion insertion--look">
        <div class="insertion__box">
          <div class="look">
            <header>
              <?php if ( $lookImageID = get_post_meta( $id, '_look_main-image_id', 1 ) ) : ?>
                <figure>
                  <a href="<?php echo get_permalink( $id ) ?>">
                    <img src="<?php echo wp_get_attachment_image_url( $lookImageID, 'look_catalog' ); ?>" srcset="<?php echo wp_get_attachment_image_url( $lookImageID, 'look_catalog_2x' ); ?> 2x" alt="<?php echo get_the_title( $id ) ?>">
                  </a>
                </figure>
              <?php endif; ?>
            </header>
            <?php /* if ( $lookProducts ) : ?>
              <div class="look__products">
                <ul class="look__products__list">
                  <?php foreach( $lookProducts as $i => $p_object ) :
                    $product = wc_get_product( $p_object->ID ); ?>
                    <li class="look__products__item"><span class="look__products__item__wrap"><span class="look__products__item__index"><?php echo ++$i; ?></span><?php if ($brands = get_product_brands($product->id)) : ?><span class="look__products__item__brand"><?php echo $brands; ?></span><span> – </span><?php endif; ?><span class="look__products__item__title"><a href="<?php echo get_permalink( $product->get_id() ); ?>"><?php echo get_the_title( $product->get_id() ); ?></a>,</span></span><span class="look__products__item__price"><?php echo $product->get_price_html(); ?></span></li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; */ ?>
            <footer>
              <a href="<?php echo get_permalink( $id ) ?>"><?php echo get_the_title( $id ) ?></a>
            </footer>
          </div>
        </div>
      </aside>
  <?php endif; ?>

  <?php $output = ob_get_clean(); return $output; ?>
  <?php
}
add_shortcode('look','looks_cpt_content_func');

/*-------------------------------------------------------------------------------*/
/* TinyMce
/*-------------------------------------------------------------------------------*/
require_once( 'tinymce/looks-tinymce.php' );


// ONLY MOVIE CUSTOM TYPE POSTS
add_filter('manage_look_posts_columns', 'ST4_columns_head_only_look', 10);
add_action('manage_look_posts_custom_column', 'ST4_columns_content_only_look', 10, 2);

// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function ST4_columns_head_only_look($defaults) {
  $defaults['directors_name'] = 'ShortCode';
  return $defaults;
}
function ST4_columns_content_only_look($column_name, $post_ID) {
  if ($column_name == 'directors_name') {
    // show content of 'directors_name' column
    echo '<input value="[look id='. $post_ID . ']" >';
  }
}