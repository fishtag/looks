<?php
/*-------------------------------------------------------------------------------*/
/*   AJAX Get Slider List
/*-------------------------------------------------------------------------------*/
function looks_grab_slider_list_ajax() {
	
	if ( !isset( $_POST['grabslider'] ) ) {
		wp_die();
	}
	else {

		$list = array();

		global $post;

		$args = array(
			'post_type' => 'look',
			'order' => 'ASC',
			'posts_per_page' => -1,
			'post_status' => 'publish'
		);

		$myposts = get_posts( $args );
		foreach( $myposts as $post ) :	setup_postdata($post);

		$list[$post->ID] = array('val' => $post->ID, 'title' => esc_html(esc_js(the_title(NULL, NULL, FALSE))) );

		endforeach;
	}

	echo json_encode($list); //Send to Option List ( Array )
	wp_die();
}

add_action('wp_ajax_looks_grab_slider_list_ajax', 'looks_grab_slider_list_ajax');

/*-------------------------------------------------------------------------------*/
/*   Frontend Register JS & CSS
/*-------------------------------------------------------------------------------*/
function looks_reg_script() {
	wp_register_style( 'looks-tinymcecss', plugins_url( 'tinymce/tinymce.css' , dirname(__FILE__) ), false, 'all');
	wp_register_script( 'looks-tinymcejs', plugins_url( 'tinymce/tinymce.js' , dirname(__FILE__) ), false );
}
add_action( 'admin_init', 'looks_reg_script' );


//-------------------------------------------------------------------------------------------------	
if ( strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post-new.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post.php' ) ) {
	
// ADD STYLE & SCRIPT
	add_action( 'admin_head', 'looks_editor_add_init' );
	function looks_editor_add_init() {

		if ( get_post_type( get_the_ID() ) != 'look' ) {

			wp_enqueue_style( 'looks-tinymcecss' );
			wp_enqueue_script( 'looks-tinymcejs' );

	?>
			<?php
		}

	}
	
// ADD MEDIA BUTOON	
	add_action( 'media_buttons_context', 'looks_shortcode_button', 1 );
	function looks_shortcode_button($context) {
		$img = LOOKS_PLUGIN_DIR .'img/look.png';
		$container_id = 'looksmodal';
		$title = 'Insert Look';
		$context .= '
		<a class="thickbox button" id="looks_shortcode_button" title="'.$title.'" style="outline: medium none !important; cursor: pointer;" >
		<img src="'.$img.'" alt="" width="20" height="20" style="position:relative; top:-1px"/>Look</a>';
		return $context;
	}
}


// GENERATE POPUP CONTENT
add_action('admin_footer', 'looks_popup_content');
function looks_popup_content() {

if ( strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post-new.php' ) || strstr( $_SERVER['REQUEST_URI'], 'wp-admin/post.php' ) ) {

if ( get_post_type( get_the_ID() ) != 'look' ) {
// START GENERATE POPUP CONTENT

?>
<div id="looksmodal" style="display:none;">
<div id="tinyform" style="width: 550px;">
<form method="post">

<div class="looks_input" id="lookstinymce_select_slider_div">
<label class="label_option" for="lookstinymce_select_slider">Look</label>
	<select class="looks_select" name="lookstinymce_select_slider" id="lookstinymce_select_slider">
    <option id="selectslookslider" type="text" value="select">- Select Look -</option>
</select>
<div class="clearfix"></div>
</div>

<div class="looks_button">
<input type="button" value="Insert Shortcode" name="looks_insert_scrt" id="looks_insert_scrt" class="button-secondary" />
<div class="clearfix"></div>
</div>

</form>
</div>
</div>
<?php 
	}
  } //END
}

?>